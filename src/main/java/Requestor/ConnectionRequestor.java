package Requestor;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

//An application that requests a connection and receives a message
public class ConnectionRequestor {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //Take in the ip address to connect to
        System.out.println("Please enter the IP address to connect to: ");
        String hostname = input.nextLine();

        //Take in the port to connect to
        System.out.println("Please enter the port to connect to: ");
        int portNumber = input.nextInt();
        input.nextLine();

        try{
            InetAddress acceptorHost = InetAddress.getByName(hostname);
            //Try to create a data socket connected to the host
            //Will fail if no one is listening
            Socket requestorSocket = new Socket(acceptorHost, portNumber);
            System.out.println("Connection requesr granted");
            //Get an input stream to read from the data socket
            InputStream inStream = requestorSocket.getInputStream();
            //Create a scanner to read input from the stream
            Scanner socketInput = new Scanner(new InputStreamReader(inStream));
            System.out.println("Waiting to read...");

            //Wait for a message from the acceptor and print is when is arrives
            String message = socketInput.nextLine();
            System.out.println("Message received: ");
            System.out.println("\t" + message);

            //Message has been received, close the datasocket
            requestorSocket.close();
            System.out.println("Data socket closed");
        }catch(UnknownHostException uhx){
            uhx.printStackTrace();
        }catch(IOException iox){
            iox.printStackTrace();
        }

    }
}
