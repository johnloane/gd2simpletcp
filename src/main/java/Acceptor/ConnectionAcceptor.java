package Acceptor;
//An application that accepts connections and
//sends a message using stream sockets

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Scanner;

public class ConnectionAcceptor {
    public static void main(String[] args) {
        //Take in input port
        Scanner input = new Scanner(System.in);
        //Take in the port to accept connections
        System.out.println("Please enter the port number that " +
                "this application should accept connections on:");
        int portNumber = input.nextInt();
        //Clear the Scanner's buffer
        input.nextLine();
        //Take the message that should be returned to the requestor
        System.out.println("Please enter the message to return to " +
                "requestors:");
        String message = input.nextLine();

        try{
            //Set up a connection
            //Create a socket for accepting connections
            ServerSocket listenSocket = new ServerSocket(portNumber);
            System.out.println("Now I am ready to accept connections");

            //Wait to accept connections
            //When a connection is accepted a data socket is created
            Socket dataSocket = listenSocket.accept();
            System.out.println("Connection accepted from ...");

            //Get an output stream to write to the data socket
            OutputStream outStream = dataSocket.getOutputStream();
            PrintWriter socketOutput = new PrintWriter(new OutputStreamWriter(outStream));

            socketOutput.println(message);

            //Flush stream - ensure that the data is actually written to the socket's stream
            socketOutput.flush();
            System.out.println("Message sent");

            //Message has been sent, close the datasocket
            dataSocket.close();
            System.out.println("Data socket closed");
            //Close the connection socket
            listenSocket.close();
            System.out.println("Listening socket closed");
        }catch(IOException iox){
            iox.printStackTrace();
        }
    }
}
